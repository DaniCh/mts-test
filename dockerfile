# Выбираем базовый образ
FROM nginx:alpine

# Копируем резюме 
#COPY resume /usr/share/nginx/html
COPY resume /var/www/html
# Копируем нжинкс конфиг
COPY nginx.conf /etc/nginx/nginx.conf

# Указываем порт, который будет слушать контейнер
EXPOSE 80

# Запускаем NGINX
CMD ["nginx", "-g", "daemon off;"]